using System.Collections.Generic;

namespace Tools
{
    public class Message
    {
        public Operation Operation { get; set; }
        public List<IdentityCard> ServicesCards { get; set; }
    }
}