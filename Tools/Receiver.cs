using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Web.Script.Serialization;
using Contracts;

namespace Tools
{
    public class Receiver : IDisposable, IReceiver
    {
        private readonly UdpClient _udpClient;
        private IPEndPoint _localEp = new IPEndPoint(IPAddress.Any, 2222);

        public Receiver()
        {
            var multicastaddress = IPAddress.Parse("239.0.0.222");

            _udpClient = new UdpClient(); // { ExclusiveAddressUse = false, EnableBroadcast = true, MulticastLoopback = false };
            _udpClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);

            _udpClient.ExclusiveAddressUse = false;

            _udpClient.Client.Bind(_localEp);
            _udpClient.JoinMulticastGroup(multicastaddress);
        }

        public void Dispose()
        {
            _udpClient.Close();
        }

        public T Receive<T>()
        {
            var data = _udpClient.Receive(ref _localEp);
            var strData = Encoding.Unicode.GetString(data);

            var serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Deserialize<T>(strData);

            Console.WriteLine("<= : " + Encoding.Unicode.GetString(data));

            return serializedResult;
        }
    }
}