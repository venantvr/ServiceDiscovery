using System.Collections.Generic;

namespace Contracts
{
    public interface IIdentitiesStore
    {
        List<IIdentityCard> Resolve();

        void Add(string route, string hostName);
    }
}