namespace Contracts
{
    public interface IIdentityCard
    {
        string HostName { get; set; }
        string[] Routes { get; set; }
    }
}