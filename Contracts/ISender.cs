namespace Contracts
{
    public interface ISender
    {
        void Send<T>(T what);

        void Dispose();
    }
}