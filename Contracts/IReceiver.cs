namespace Contracts
{
    public interface IReceiver
    {
        T Receive<T>();

        void Dispose();
    }
}